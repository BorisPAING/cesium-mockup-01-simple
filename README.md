Cesium Mockup - 01 - Simple
===

## A qui s'adresse ce projet ?

- Aux gens qui souhaient proposer des modifications de Cesium efficacement (en créant des prototypes) mais qui ne connaissent ni Javascript ni AngularJS ni Ionic.
- Aux UX designers
- Aux graphistes

## Quel est l'intérêt ?

Le code de ce projet est épuré de tout ce qui est compliqué dans le code original de Cesium : 
- les directives AngularJS sont enlevées, 
- les variables, boucles et conditions sont remplacées par des données d'exemple

Ce code contient également moins de fichiers, pour la simple et bonne raison que tous les écrans ne sont pas présent ni toutes les interactions implémentées. Idéal pour rentrer en douceur dans le code de Cesium et se familiariser avec l'architecture et la terminologie.

## De quoi ai-je besoin ?

* D'un serveur, local ou distant (essayer de lancer l'application via file:/// ne fonctionnera pas)
* De connaissances en HTML et CSS

## Qu'est-ce que je gagne à utiliser ces fichiers plutôt que les originaux de Cesium ?

Ceux-là sont plus facile à comprendre et vous permettront de rentrer en douceur dans le code, même si vous souhaitez aller plus loin par la suite.

Vous n'avez pas besoin de savoir ce qu'est : 
- Gulp
- Cordova
- Bower

Vous n'êtes pas obligé d'installer quoi que ce soit.

Vous n'avez pas besoin de taper une seule ligne de commande.

Vous n'avez pas besoin de bien comprendre AngularJS.

## Est-ce qu'il y a quand même des choses que je dois apprendre ?

Oui et non.

Si la modification que vous souhaitez apporter est mineure, vos seules connaissances en HTML et CSS peuvent être suffisantes.

Cependant, pour des modifications qui demandent la création de fichier, il vous faudra comprendre le fonctionnement de AngularUI-router, ce qui ne nécessite pas forcément de comprendre AngularJS.

### AngularUI-router

AngularUI-router est ce qui sert à dire à l'application quels sont les chemins où aller chercher les templates.

C'est à cause d'AngularUI-router que vous verrez dans le code source des attributs ```ui-sref``` au lieu des attributs ```href```.

Les attributs ```ui-sref``` permettent d'appeler des états, plutôt que de dirigier vers une URL.

Si vous comprenez l'anglais, je vous conseille de regarder la [vidéo de présentation](https://www.youtube.com/watch?v=dqJRoh8MnBo) d'un des créateurs du module.

Bien qu'elle ne vous sera probablement pas d'une grande utilité, vous pouvez trouver la documentation à l'adresse suivante : 
https://github.com/angular-ui/ui-router/wiki

### Ionic framework v1

Ionic est un cadriciel ("framework" comme disent les anglophones) qui permet de créer rapidement des petites applications au design sympa.

Il ajoute notamment une surcouche de CSS, basée sur le material design, qui permet d'accéder à des classes pour styliser rapidement des éléments : 
* listes de choix
* cases à cocher qui ont un aspect plus "bouton"
* etc.

Ionic inclut entre autres AngularUI-router, qui utilise le système d'états.

#### Prenez la v1 !

Cesium v1 utilise la version 1 de Ionic Framework qui, contrairement aux versions suivantes, est basée sur AngularJS.

Et vous devez donc toujours vous référer à la documentation de la v1 de Ionic, à laquelle vous peinerez à accéder via les menus du site. 

La documentation se trouve ici : 
https://ionicframework.com/docs/v1/

Vous trouverez un très bon tuto sur Ionic chez Grafikart : 
https://www.grafikart.fr/tutoriels/ionic-framework-641

#### Les onglets dans Ionic

Vous trouverez peut-être que la gestion des onglets est un peu contre-intuitive.En cas de difficulté, voici de la documentation :
* https://www.thepolyglotdeveloper.com/2014/12/understanding-tabs-ionic-framework/
* https://ionicframework.com/docs/v1/api/directive/ionTabs/

### Ionicons

Ionicons est un ensemble d'icônes inclut par défaut dans Ionic.

Tous les icônes du framework Ionic actuel (v4) ne sont pas disponibles dans la v1. 

Référez-vous donc à la documentation de la v1, que vous trouverez ici : 
https://ionicons.com/v1/




