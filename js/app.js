// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider){
	
	$urlRouterProvider.otherwise('/app/home');
	
	/*----------------------- Base -----------------------*/
	
	$stateProvider.state('app', {
		url: "/app",
		abstract: true, 
		templateUrl: 'templates/menu.html'
	});

	/*----------------------- Accueil -----------------------*/
	
    $stateProvider.state('app.home', {
		url: "/home",
		templateUrl: 'templates/home/home.html'
	});
	
	/*----------------------- Mon compte -----------------------*/
	
	$stateProvider.state('app.account', {
		url: '/account', 
		abstract: true,
		templateUrl: 'templates/account/account.html'
	});
	
	$stateProvider.state('app.account.key', {
		url: '/key', 
		views: {
			'account-key': {
				templateUrl: 'templates/account/tabs/key.html'
			}
		}
	});
	
	$stateProvider.state('app.account.history', {
		url: '/history', 
		views: {
			'account-history': {
				templateUrl: 'templates/account/tabs/history.html'
			}
		}
	});
	
	$stateProvider.state('app.account.my', {
		url: '/my', 
		views: {
			'account-my': {
				templateUrl: 'templates/account/tabs/my.html'
			}
		}
	});
	
	
	/*----------------------- Portefeuilles -----------------------*/
	
	
	$stateProvider.state('app.view_wallets', {
        url: "/wallets",
		templateUrl: "templates/wallet/list/view_wallets.html"
	});
	
	
	
	/*----------------------- Annuaire -----------------------*/
	
	$stateProvider.state('app.wot', {
		url: '/wot',
		templateUrl: 'templates/wot/wot.html'
	});
	$stateProvider.state('app.wot_contacts', {
		url: '/wot/contacts', 

				templateUrl: 'templates/wot/tabs/contacts.html'

	});
	$stateProvider.state('app.wot_search_name', {
		url: '/wot/search/name/', 
		templateUrl: 'templates/wot/tabs/lookup_name.html'

	});
	$stateProvider.state('app.wot_search_key', {
		url: '/wot/search/key/', 
		templateUrl: 'templates/wot/tabs/lookup_key.html'
	});
	
	
	
	/*----------------------- Messages -----------------------*/
	
	$stateProvider.state('app.user_message', {
		url: '/usesr/message', 
		abstract: true,
		templateUrl: 'templates/message/lookup.html'
	});
	
	$stateProvider.state('app.user_message.tab_inbox', {
		url: '/inbox', 
		views: {
			'tab_inbox': {
				templateUrl: 'templates/message/tabs/tab_list.html'
			}
		}
	});
	
	$stateProvider.state('app.user_message.tab_outbox', {
		url: '/outbox', 
		views: {
			'tab_outbox': {
				templateUrl: 'templates/message/tabs/tab_list.html'
			}
		}
	});
	
	
	/*----------------------- Notifications -----------------------*/
	
	$stateProvider.state('app.view_notifications', {
		url: '/notifications',
		templateUrl: 'templates/notification/view_notifications.html'
	});
	

	/*----------------------- Paramètres -----------------------*/
	
	
	$stateProvider.state('app.settings', {
		url: '/settings',
		templateUrl: 'templates/settings/settings.html'
	});
	
	
	/*----------------------- Actualités -----------------------*/
	
	$stateProvider.state('app.news', {
		url: '/news', 
		abstract: true,
		templateUrl: 'templates/news/news.html'
	});
	
	$stateProvider.state('app.news.dev', {
		url: '/dev', 
		views: {
			'news-dev': {
				templateUrl: 'templates/news/tabs/dev.html'
			}
		}
	});
	
	$stateProvider.state('app.news.patrons', {
		url: '/patrons', 
		views: {
			'news-patrons': {
				templateUrl: 'templates/news/tabs/patrons.html'
			}
		}
	});
	
	$stateProvider.state('app.news.stats', {
		url: '/stats', 
		views: {
			'news-stats': {
				templateUrl: 'templates/news/tabs/stats.html'
			}
		}
	});
	
	/*----------------------- Encourager -----------------------*/
	
	$stateProvider.state('app.support', {
		url: '/support',
		templateUrl: 'templates/support/support.html'
	});
	
	
	/*----------------------- A propos -----------------------*/
	
	$stateProvider.state('app.about', {
		url: '/about',
		templateUrl: 'templates/about.html'
	});
});